import ctypes
import sys
import os

ARRAY_SIZE = 8

# Print statment
def printArrs():
    # Print the src array
    print("Src:")
    for val in c_JIT.getSrc().contents:
        print(f'{val} ', end = "")

    # Print the dest array
    print("\nDest:")
    for val in c_JIT.getDest().contents:
        print(f'{val} ', end = "")
    
    # Print the dest array
    #print("\nVars:")
    #for val in c_JIT.getVars().contents:
    #    print(f'{val} ', end = "")
    print("\n")

# Complie and load the c code
if 0 != os.system(f'cc -fPIC -shared -mavx2 -o C_JIT.so C_JIT.c'):
    sys.exit("Failed to complie C code")
c_JIT = ctypes.cdll.LoadLibrary("./C_JIT.so")

# Setup return types
c_JIT.getDest.restype  = ctypes.POINTER(ctypes.c_float * ARRAY_SIZE)
c_JIT.getSrc.restype   = ctypes.POINTER(ctypes.c_float * ARRAY_SIZE)
c_JIT.getVars.restype  = ctypes.POINTER(ctypes.c_float * ARRAY_SIZE)

# Set argument types
c_JIT.setSource.argtypes  = [ctypes.c_float * ARRAY_SIZE, ctypes.c_size_t]
c_JIT.Load.argtypes       = [ctypes.c_int                                ]
c_JIT.Store.argtypes      = [ctypes.c_int               , ctypes.c_int   ]
c_JIT.VecLoad.argtypes    = [ctypes.c_int                                ]
c_JIT.VecStore.argtypes   = [ctypes.c_int                                ]
c_JIT.VecShuffle.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]
c_JIT.VecBlend.argtypes   = [ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int]

######################
#    Testing area    #
######################
# Give src array
indata = [5,6,7,8,9,10,11,12]
arr = (ctypes.c_float * ARRAY_SIZE)(*indata)
c_JIT.setSource(arr, 8)

c_JIT.allocMem()

# Runing a test for the normal instruction
print("Test 1")
# Load for normal
for x in range(0, 8):
    c_JIT.Load(x)

# Store for normal
for x in range(0, 8):
    c_JIT.Store(x, 7 - x)
printArrs()

c_JIT.resetMem()

# Runing a test for the vector instruction
print("Test 2")
c_JIT.VecLoad(0)

c_JIT.VecShuffle(0, 0, 0, ctypes.c_int(12345670))

c_JIT.VecStore(0)

printArrs()