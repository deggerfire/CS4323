# Nathan is a nerd

## True

https://github.com/deggerfire/CS4323

## Running the code
- python3 main.py \<shuffle order\>
- python3 main.py 3 1 0 2 4 5 7 6

## Limitations
- Shuffle order should be 8 long (not hard to extend, but runtime gets worse)
- Shuffle across lanes is not allowed (lane length = 4, exceptions exist if you know what you are doing)


## Message from Daniel

The avx_tutorial folder contains my AVX homework from last semester.

Call "make" in that folder to see the results. Some of the answers are not ideal, but I got all of them correct except #10.

hw2_code.c contains all the important stuff. The other .c files were for testing and much of it is broken, so properly just ignore it.

The goal of this folder is to give you a better understanding of AVX instruction syntax.
