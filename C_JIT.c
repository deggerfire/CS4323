#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>

// The maximin number of stored values
#define ARRAY_SIZE 8

// The array of store values
float* vars = NULL;

// The source array
float* src = NULL;
// The destination array
float* dst = NULL;

// The __m256 array variable
__m256* registers;

// Ues calloc to allocate arrays
void allocMem(){
    vars      = calloc(ARRAY_SIZE , sizeof(float));
    dst       = calloc(ARRAY_SIZE , sizeof(float));
    registers = malloc(ARRAY_SIZE * sizeof(__m256));
}

// Free memory arrays
void freeMem(){
    free(vars);
    free(dst);
    free(registers);
}

// Frees then allocates arrays
void resetMem(){
    freeMem();
    allocMem();
}

//----------------------------------------//
//         Util Array methods             //
//----------------------------------------//

// Loads values into the source array
void setSource(float* input, int size){
    src = calloc(ARRAY_SIZE, sizeof(float));
    for(int i = 0; i < size; i++){
        src[i] = input[i];
    }
    printf("\n");
}

// Frees the source array
void freeSource(){
    free(src);
}

// Returns the dest array
float* getDest(){
    return dst;
}
// Returns the vars array
float* getVars(){
    return vars;
}
// Returns the source array
float* getSrc(){
    return src;
}
//----------------------------------------//
//          Load Instructions             //
//----------------------------------------//

// Load instrustion for normal
void Load(int offset){
    //src_0      = src[0]
    vars[offset] = src[offset];
}

// VecLoad instrustion
void VecLoad(int i){
    registers[i] = _mm256_loadu_ps(src);
}

//----------------------------------------//
//         Store Instructions             //
//----------------------------------------//

// Store instrustion
void Store(int offset, int varIdx){
    //dst[1]    = src_0
    dst[offset] = vars[varIdx];
}

// VecStore instrustion
void VecStore(int i){
     _mm256_storeu_ps(dst, registers[i]);
}

//----------------------------------------//
//         Register instrustions          //
//----------------------------------------//

// VecShuffle instrustion !not working!
void VecShuffle(int out_reg, int reg_in_1, int reg_in_2, int imm8){
    switch(imm8){
        case 01234567: registers[out_reg] = _mm256_shuffle_ps(registers[reg_in_1], registers[reg_in_2], 01234567);break;
        case 76543210: registers[out_reg] = _mm256_shuffle_ps(registers[reg_in_1], registers[reg_in_2], 76543210);break;
        /**6435 - 2 more of these**/
    }
}

// VecBlend instrustion !not working!
void VecBlend(int out_reg, int reg_in_1, int reg_in_2, int imm8){
    registers[out_reg] = _mm256_blend_ps(registers[reg_in_1], registers[reg_in_2], 0b1010101);
}

//----------------------------------------//
//         Main (complier requires)       //
//----------------------------------------//
int main(){
    return 0;
}